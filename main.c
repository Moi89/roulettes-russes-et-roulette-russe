#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <stdbool.h>

//J'initialise votre vie !
bool isAlive = true;

int testBetAmount(int bet, int poolPlayer)
{
    // Jetons pari�s doivent �tre compris entre 0 et 25 et �tre inf�rieurs au montant total dispo
     while (bet > poolPlayer || bet == 0 || bet > 25)
     {
        if (bet > poolPlayer)
        {
            printf("Vous ne pouvez pas miser plus que ce que vous avez. Recommencez ! \n");
        }
        else if (bet >25)
        {
            printf("Vous ne pouvez pas miser plus de 25 jetons a la fois. Recommencez ! \n");
        }
        else
        {
            printf("Vous ne pouvez pas miser 0. Recommencez ! \n");
        }

        scanf("%d",&bet);
     }
     return bet;
}

int askAndTestBet ()
{
    int nbPlayer = 2;

    // On ne peut parier que sur 0 ou 1
    while (nbPlayer != 0 && nbPlayer != 1)
    {
        printf("Choisissez ou va s'arreter la bille entre : pair (0) impair (1)\n");
        scanf("%d",&nbPlayer);
    }
    return nbPlayer;
}
int roulette (int poolPlayer)
{
    int bet = 0;
    int nbPlayer = 0;

    // On joue tant que les jetons ne sont pas a 0 ou superieurs a 100
    while (poolPlayer > 0 && poolPlayer < 100)
    {
        printf("Vous disposez de %d jetons, combien souhaitez-vous miser ?\n",poolPlayer);
        scanf("%d",&bet);

        // V�rification du nombre de jetons pari�s
        bet = testBetAmount(bet,poolPlayer);

        //V�rification du parie effectu� par le joueur
        nbPlayer = askAndTestBet();

        printf("Vous avez parie ");

        if (nbPlayer == 0)
        {
            printf("pair.\n");
        }
        else
        {
            printf("impair.\n");
        }

        //Chiffre random entre 0 et 36
        int nbComp = rand()%37;

        printf("Rien ne va plus ...\n\n");

        Sleep(1000);

        printf("La roulette s'arrete sur le %d.\n", nbComp);

        if (nbPlayer == nbComp%2)
        {
            poolPlayer = poolPlayer + bet*2;
            printf("Vous gagnez !\nVos gain sont de : %d.\n\n", bet*2);
        }
        else
        {
            poolPlayer = poolPlayer - bet;
            printf("Vous perdez !\n\n");
        }
    }
    return poolPlayer;
}

int russianRoulette (int poolPlayer)
{
    //Cr�ation du tableau repr�sentant le barillet
    int tab[6];
    int i = 0;
    int playerChoice = 1;

    //Remplissage du tableau � 0 pour �viter d'avoir des valeur al�atoires
    for (i = 0 ; i < 6 ; ++i)
    {
        tab[i] = 0;
    }

    //Position de la balle dans le tableau randomis�e
    int bulletPos = rand()%6;
    tab[bulletPos] = 1;

    //On sort de cette boucle lorsque l'on a gagn� 100 jetons � la roulette russe ou que l'on souhaite repasser � la roulette ... ou que l'on est mort
    while (playerChoice == 1 && poolPlayer < 100 && isAlive)
    {
        //Attente de 1s avant le d�nouement pour build up un peu de suspens
        Sleep(1000);

        printf("Votre doigt est force de presser la detente ...\n\n");

        Sleep(3000);

        if (tab[0] == 1)
        {
            printf("Le bruit de la detonation n'a pas le temps d'atteindre vos oreilles que votre vision s'assombrit et le monde disparait. Vous etes mort. Votre famille paiera vos dettes a votre place. \n\n           !!!!! GAME OVER !!!!! \n");
            printf("                       ,-----------.         `.           ,--'\n                     ,'             `.      ,-;--        _.-\n               pow! /                 | ---;-'  _.=.---''\n  +-------------+  ;                   |---=-----'' _.-------\n  |    -----    |--|    X        X     |-----=---:i-\n  +XX|'i:''''''''  :                   ;`--._ ''---':----\n  /X+-)             |                 /      ''--._  `-\n .XXX|)              `.  `.     ,'  ,'             ''---.\n   X|/)                `.  '---'  ,'                     `-\n     |                   `---+---'\n      `                      |\n       |                     |\n         `-------------------+\n");
            isAlive = false;
        }
        else
        {
            printf("Un clic salvateur retentit, et l'on pousse 20 jetons devant vous tandis que le revolver s'eloigne ... \nAllez-vous continuer de parier votre vie ou ces jetons nouvellement acquis ?\n\n1. Continuer la roulette russe 2. Retour a la roulette \n");
            poolPlayer = poolPlayer + 20;
            printf("Vous disposez de %d jetons\n\n",poolPlayer);
            tab[bulletPos] = 0;
            bulletPos = bulletPos - 1;
            tab[bulletPos] = 1;
            scanf("%d",&playerChoice);
        }
    }
    return poolPlayer;
}

int main()
{
    int poolPlayer = 10;
    //Initialisation du random
    srand(time(NULL));

    printf("Vous vous eveillez difficilement au milieu d'un casino, ligote a une chaise face a la roulette et surveille par trois armoires a glace. Un papier rature devant vous indique, Dette : 100 jetons ... il va falloir jouer pour payer !\n\n");

    //Premier tour de roulette obligatoire
    poolPlayer = roulette (poolPlayer);

    //On sort de la roulette de d�part seulement quand on n'a plus de jetons ou que l'on en a 100 et on continue donc de jouer tant que ces condtions sont toujours vraies
    while (poolPlayer <=100 && isAlive)
    {
        if (poolPlayer == 0)
        {
            printf("Vous n'avez plus de jetons ... L'un des gros bras present vous attrape la main, vous force a saisir le revolver et l'approche de votre tempe. Vous entendez les paris sur votre survie courir dans la salle \n\n");
            poolPlayer = russianRoulette(poolPlayer);
        }
        else
        {
            poolPlayer = roulette(poolPlayer);
        }
    }

    // Il est possible de d�passer 100 jetons donc on pr�voit un sup�rieur ou �gal
    if (poolPlayer >=100)
    {
        printf("Votre dette est payee et vous etes expulse du casino. Felicitations, vous avez survecu ! \n");
    }

    return 0;
}
